﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Osmos.Discovery
{
    public class ApiDescription
    {
        public string HttpMethod { get; set; }
        public string Route { get; set; }
    }
    public class Group
    {
        public string Name { get; set; }
        public ApiDescription[] ApiDescriptions { get; set; }
    }

    public static class Discovery
    {
        public static List<Group> Get(HttpConfiguration Configuration)
        {
            var model = Configuration.Services.GetApiExplorer().ApiDescriptions;

            var apiGroups = model.ToLookup(api => api.ActionDescriptor.ControllerDescriptor);

            var groups = new List<Group>();
            foreach (var group in apiGroups)
            {
                var apiDescriptions = new List<ApiDescription>();

                groups.Add(new Group
                {
                    Name = group.Key.ControllerName,
                    ApiDescriptions = group.Select(api => new ApiDescription
                    {
                        HttpMethod = api.HttpMethod.Method,
                        Route = api.RelativePath
                    }).ToArray()
                });
            }

            return groups;
        }
    }
}
